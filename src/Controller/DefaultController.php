<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Hub;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/', name: 'default')]
class DefaultController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('base.html.twig');
    }


    #[Route('/publish', name: 'publish')]
    public function publish(Hub $hub): Response
    {
        $update = new Update(
            'http://example.com/books/1',
            json_encode(['message' => 'Published to general topic'])
        );

        $hub->publish($update);

        return $this->json([
            'message' => 'Message published.',
        ]);
    }

}
