import React from 'react'
import {Link, Route} from 'react-router-dom'
import {Routes} from 'react-router'

import Home from './Home'
import Users from './Users'
import Posts from './Posts'

const App = () => {

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link className={'navbar-brand'} to={'/'}> Symfony React Project </Link>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link className={'nav-link'} to={'/'}> Home </Link>
            </li>

            <li className="nav-item">
              <Link className={'nav-link'} to={'/posts'}> Posts </Link>
            </li>

            <li className="nav-item">
              <Link className={'nav-link'} to={'/users'}> Users </Link>
            </li>
          </ul>
        </div>
      </nav>

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/posts" element={<Posts />} />
        <Route path="/users" element={<Users />} />
      </Routes>
    </div>
  )
}

export default App
